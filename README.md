
- Voici  une architecture des répertoires du projet :   

```
.
├── alertmanager
│   ├── conf
│   │   ├── alertmanager.yml
│   │   └── docker-entrypoint.sh
│   └── Dockerfile
│
├── caddy
│   └── Caddyfile
│
├── grafana
│   ├── dashboards
│   │   ├── swarmprom-nodes-dash.json
│   │   ├── swarmprom-prometheus-dash.json
│   │   └── swarmprom-services-dash.json
│   ├── datasources
│   │   └── prometheus.yaml
│   ├── Dockerfile
│   └── swarmprom_dashboards.yml
│
├── node-exporter
│   ├── conf
│   │   └── docker-entrypoint.sh
│   └── Dockerfile
│
├── prometheus
│   ├── conf
│   │   ├── docker-entrypoint.sh
│   │   ├── prometheus.yml
│   │   └── weave-cortex.yml
│   ├── Dockerfile
│   └── rules
│       ├── swarm_node.rules.yml
│       └── swarm_task.rules.yml
│
└── docker-compose.ym
```


Une fois le projet est cloné, lancer :   

`docker stack deploy -c docker-compose.yml <env>`
